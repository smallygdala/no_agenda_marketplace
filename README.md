# No Agenda Marketplace

A directory of No Agenda Producer run businesses.


## Website

- [No Agenda Marketplace](https://marketplace.yanoagenda.com/)
- [Site was based on this template](https://github.com/google/eleventy-high-performance-blog)

## Contributing

### 1. Clone the repo

```
git clone https://gitlab.com/smallygdala/no_agenda_marketplace.git
```

### 2. Navigate to the directory

```
cd no_agenda_marketplace
```

### 3. Install dependencies

```
npm install
```

### 4. Build, serve, watch and test

```
npm run watch
```

### 5. Add a business page

Copy a file in the `business/` folder and update it with the new business information

```
cd business
cp _blank.md.template my_business.md
vim my_business.md
```

### 6. Create a Merge Request

https://gitlab.com/smallygdala/no_agenda_marketplace/-/merge_requests/new
