---
title: C.M. Gromoll
description: Emotional fitness and deprogramming coach
author: Smallygdala
date: 2022-09-20
tags:
  - shop
  - life coach
url: https://cmgromoll.com/
location: Online
profile: https://noagendasocial.com/@Cgromoll
image:
---

My goal is to help you learn to take control of your life.  Transform your understanding of self so that you can harness that power for positive change.

