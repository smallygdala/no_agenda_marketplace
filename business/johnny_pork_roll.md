---
title: Johnny's Pork Roll Truck
description: Pork Roll & Coffee
author: Smallygdala
date: 2022-02-13
tags:
  - shop
  - pork
  - sandwiches
  - coffee
url: https://johnnyporkroll.com/
location: Red Bank, New Jersey
profile: https://social.stracknet.com/@dan
image:
---

Pork sammies and coffee too!
