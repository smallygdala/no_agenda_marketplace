---
title: Chefs Catch
description: Seafood Cafe on The Tamar River Tasmania
author: Smallygdala
date: 2022-09-08
tags:
  - shop
  - food
  - seafood
url: https://www.chefscatch.com.au/
location: Tasmania, Australia
profile: https://noagendasocial.com/@ChefsCatch
---

Just the best little fish and chip shop in Tasmania. Great food, no bugs.

