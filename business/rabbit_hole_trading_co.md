---
title: Rabbit Hole Trading Co.
description: Functional Mushrooms and Herbs
author: Smallygdala
date: 2022-03-02
scheduled: 2022-03-02
tags:
  - shop
  - mushrooms
  - herbs
layout: layouts/business.njk
url: https://www.rabbitholetradingco.com/
image: /img/business/rabbit_hole_trading_co.png
location: Online
profile: https://noagendasocial.com/@GrumpyGreenGuy
---

Chrissy has obtained the level of Clinical Herbalist, and is continuing her education to become a Medical Herbalist. Her fascination with the power that plants and specifically mushrooms hold started when she became an adult, and had to take care of her own health. This sparked a love for all things nature which she shares with anyone who will listen!
