---
title: No Agenda Shop
description: No Agenda Gear
author: Smallygdala
date: 2022-03-02
scheduled: 2022-03-02
tags:
  - shop
  - stickers
  - clothes
  - art
layout: layouts/business.njk
url: https://noagendashop.com/
image: /img/business/no_agenda_shop.png
location: Online
profile: 
---

The No Agenda Shop was created as a way to fund and promote the No Agenda Show. Our goal is to create bold designs that listeners will be proud to own and drive new listeners to the show. Since our founding in 2016 we have donated a portion of all sales directly to the No Agenda Show.
