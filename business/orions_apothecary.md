---
title: Orion's Apothecary
description: Fire Cider, tea, honey-fermented garlic, and tinctures. We are "Sirius" about wellness!
author: Practical
date: 2023-01-05
tags:
  - shop
  - medicinal
url: https://orions-apothecary.com/
location: Online and in-store at 270 E 29th Street, Loveland, CO 80538
profile: https://noagendasocial.com/@spideycents
image:
---

Orion's Apothecary creates delicious and effective plant-based remedies, based on recipes passed down for generations.  Not only can you help get yourself back to health and keep yourself healthy, but you can also use these in your cooking as well. Honey-fermented garlic makes a delicious marinade for salmon, chicken, or pork.  Fire cider can be used in salad dressing, brisket soak, smoker spray, or in any recipe that calls for apple cider vinegar.
https://www.instagram.com/orions_apothecary/
