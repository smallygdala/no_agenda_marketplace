---
title: YES Cacao
description: Chocolate Powered By Botanicals
author: Smallygdala
date: 2022-08-16
tags:
  - shop
  - chocolate
url: https://www.yescacao.com/
location: Online
profile: https://www.instagram.com/yescacao/
image:
---

Chocolate Powered By Botanicals

YES, it’s organic, raw, wild-harvested cacao. YES, it’s blended with function-specific formulas. YES, eat it everyday, benefits are cumulative with regular use.

Dark. Wild Harvested. Cacao.

Use coupon code ITM
