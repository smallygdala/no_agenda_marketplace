---
title: Fox Knob Farm
description: Pork and derivative products
author: Practical
date: 2023-04-26
tags:
  - shop
url: https://foxknobfarm.com/
location: Jonesville, NC
profile: https://noagendasocial.com/@reesoteric
image: /img/business/fox_knob_farm.png
---

Nothing says “OTG” like a freezer full of quality meat! Christopher & Deborah (who met because of No Agenda) run Fox Knob Farm near Elkin, NC and raise heritage breed pastured pork the old-fashioned way, no meds and nothing toxic. Reserve your whole or half share today, and try our homemade lard body cream too! An “ITM” when you contact us gets you 10% off your order. Learn more at foxknobfarm.com and contact us at noagenda@foxknobfarm.com.
