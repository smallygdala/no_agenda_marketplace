---
title: CBD Loveland
description: Small-batch, Colorado-grown CBD products for people and pets
author: Practical
date: 2023-01-05
tags:
  - shop
  - cbd
url: https://www.cbdloveland.com/
location: Online and in-store at 270 E 29th Street, Loveland, CO 80538
profile: https://noagendasocial.com/@spideycents
image: /img/business/cbd_loveland.png
---

Canna World Market (aka CBD Loveland) was opened to provide an option for people who are looking for good information and high quality products in a comfortable and low-pressure setting. We will take the time to walk you through options, and make excellent recommendations on how to work with your pain, stress, or sleep issues.
https://www.instagram.com/cannaworldmarketloveland
