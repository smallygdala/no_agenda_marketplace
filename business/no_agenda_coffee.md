---
title: No Agenda Coffee
description: A hot cup of amygdala-shrinking goodness
author: Smallygdala
date: 2022-03-02
scheduled: 2022-03-02
tags:
  - shop
  - coffee
layout: layouts/business.njk
url: https://noagendacoffee.com/
image: /img/business/no_agenda_coffee.png
location: Online
profile: 
---

HEY GUYS! We're two NA Knights in Tennessee who love roasting coffee while listening to Crackpot and Buzzkill. We launched this little passion project as a value-for-value proposition; buy our coffee and 20% goes to NA, and you'll be supporting our small biz. We hope you love every sip, along with our bag designs - it's what we drink while listening, too. Adios mofos!

-Sir Spro and Sir Bean Brewer of the South