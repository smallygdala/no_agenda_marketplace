---
title: Held Gear
description: American-made apparel. Currently its all belts/straps, but there is more in the dream than that.
author: Smallygdala
date: 2022-09-20
tags:
  - shop
  - apparel
url: https://www.heldgear.com/
location: Online
profile: https://noagendasocial.com/@seanongley
image:
---

American-made apparel. Currently its all belts/straps, but there is more in the dream than that.
