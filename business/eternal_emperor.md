---
title: EternalEmperor.com
description: Fiction Author
author: Practical
date: 2023-07-26
tags:
  - shop
  - books
  - author
  - writing
  - fiction
url: https://eternalemperor.com/
location: Online
profile: https://noagendasocial.com/@ThePM
image: 
coupon: 
---

EternalEmperor.com is the home for a writer focusing on underserved topics such as Zombies, Science Fiction, NASCAR Science Fiction, and No Agenda Themed Special Operations stories.  All are 100% Fiction and spring from the mind of a Dude Named Ben, now a manager of Dudes Named Ben.  I am faced with using my dwindling spare time to watch what other people want me to see or write what I want to read.  
