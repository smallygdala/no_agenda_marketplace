---
title: Hollow Books
description: We specialize in making concealed carry gun cases from real books that allow you to keep your personal protection close by. We also take requests.
author: Smallygdala
date: 2022-03-02
tags:
  - shop
  - books
url: https://hollowbooks.com/
image: /img/business/hollow_books.png
location: Online
profile: https://noagendasocial.com/@Hollowbooks
---

For ten years we've been meticulously creating these hand-crafted works of art and now we've made it easier than ever to order your very own custom hollow book. Mail in your own book or choose one from our inventory, we can make a custom book to hide whatever you want. We specialize in cutting to exact gun-manufacturer specifications. Every custom book order is unique and every order is made just for you.
