---
title: Ernest Benoit Music
description: AUTHENTIC ORIGINALS + QUALITY COVERS
author: Smallygdala
date: 2022-03-02
scheduled: 2022-03-02
tags:
  - service
  - music
layout: layouts/business.njk
url: https://www.framminghq.com/
image: /img/business/ernest_benoit_music.png
location: Online
profile: https://noagendasocial.com/@Framming
---

What type of music do I play? In addition to my original acoustic rock songs (found on my 2014 and 2016 releases 'Coaster' and 'Grey Morning Light') I mix in the hits you know and love along with the deep cuts from artists ranging from Johnny Cash to Queen and Tom Petty to AC/DC.

