---
title: Don't CA My USA
description: Anti-California sloganized merchandise
author: Practical
date: 2022-09-16
tags:
  - shop
url: https://dontcamyusa.com/
location: Online
profile: https://noagendasocial.com/@saltymedic
image:
---

We sell stickers and magnets you can use to tell the California refugees invading your home state that you don't want them turning your home into the horrible state that they just left. We sell stickers for more than 10 different states. Don't California my USA!
