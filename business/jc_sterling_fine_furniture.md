---
title: J.C. Sterling Fine Furniture
description: One of kind hand crafted furniture
author: Smallygdala
date: 2022-03-02
scheduled: 2022-03-02
tags:
  - shop
  - furniture
layout: layouts/business.njk
url: https://jcsterling.com/
image: /img/business/jc_sterling_fine_furniture.png
location: Millmont, PA
profile: https://noagendasocial.com/@Woodbutcher
---

J.C. Sterling Fine Furniture stands apart from the rest because I believe your furniture should be a beautiful work of art that connects you to nature and to the future generations that will inherit your investment. When it comes to natural wood furniture, our edge furniture is made from solid wood slab pieces, making for incredibly sturdy furniture.

