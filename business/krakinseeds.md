---
title: Krakin Seeds
description: Buy and sell medicinal seeds
author: Smallygdala
date: 2023-01-19
tags:
  - shop
  - cannabis
  - seeds
url: https://krakinseeds.com/
location: Online
profile: https://noagendasocial.com/@Skizzle420
image:
---

Here at Krakin Seeds our medicinal roots keep us committed to organic practices and sustainable farming. We strive to bring you some of the worlds best genetics from some of the worlds best breeders. All of our inventory has been tested for stability and germination to ensure quality results. We are continuously developing new strains to bring you new options. Stay tuned for future releases.
