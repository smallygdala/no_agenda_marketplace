---
title: Wild Oak Wellness
description: Holistic Medicine and Integral Healing
author: Practical
date: 2023-01-02
tags:
  - service
  - medicinal
url: https://www.wildoakwellness.com
location: 520 E Coeur d’Alene Ave, Unit 5, CDA, ID 83814
profile: https://noagendasocial.com/@Elder_Oak
image:
---

Dr. David Campbell, DSOM, LAc. specializes in restoring health and wellbeing through Acupuncture, manual therapy, Herbal Medicine, dietary & lifestyle changes, as well as personal growth & empowerment. David focuses on Integral Healing that encompasses the whole person, including the structural, functional, mental, emotional, and spiritual layers of being.
