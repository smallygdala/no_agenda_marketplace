---
title: Sweetwater Counseling
description: Counseling and therapy services
author: Practical
date: 2023-07-17
tags:
  - service
  - counseling
  - therapy
  - psychology
url: https://sweetwatercounselingsewickley.com/
location: Sewickley, Pennsylvania
profile: https://noagendasocial.com/@LadySignaledVirtue
image:
---

Evidence-based psychological services in Pittsburgh. Amygdala shrinking included, independent thinking guaranteed.
