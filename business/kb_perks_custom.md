---
title: KB Perks Custom
description: HAND CUSTOMIZED DRINKWARE, KEYCHAINS AND MORE
author: Smallygdala
date: 2022-03-06
tags:
  - shop
  - drinkware
  - keychains
layout: layouts/business.njk
url: https://kbperkscustom.com/
image: 
location: Online
profile: https://noagendasocial.com/@Tula
---

KB Perks started as a result of lockdowns and losing all other income opportunities. Now, we are proud and happy as clams to make beautiful and fun customized items for our great customers! Who knew that losing so much would create something so awesome?

