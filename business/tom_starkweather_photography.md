---
title: Tom Starkweather Photography
description: NYC Photographer, Audio Producer, Podcaster, EOS Mixer, No Agenda Knight
author: Smallygdala
date: 2022-03-02
scheduled: 2022-03-02
tags:
  - service
  - photography
layout: layouts/business.njk
url: http://www.tomstarkweather.com/
image: /img/business/tom_starkweather_photography.png
location: New York City, New York
profile: https://noagendasocial.com/@melodiousowls
---

Tom Starkweather began photographing for his hometown newspaper while in high school. Starkweather’s interest in photographing professionally took him to the Rochester Institute of Technology where he received a BFA in Photojournalism in 2006. Tom is interested in capturing the surprising juxtapositions and singular moments he encounters in New York City, his home for the last ten years. He is fascinated by technology, neighborhood culture, and the spontaneous moments that fuse them. When he is not photographing, Tom produces music in the form of personal compositions as well as feature and short film soundtracks.  
