---
title: Lavender Blossoms
description: All Natural Ingredients • Promoting Healing of The Body & Mind • Michigan-Based Lavender Farm
author: Smallygdala
date: 2022-03-02
scheduled: 2022-03-02
tags:
  - shop
  - cbd
layout: layouts/business.njk
url: https://www.lavenderblossoms.org/
image: /img/business/lavender_blossoms.png
location: Online
profile: https://www.instagram.com/lavenderblossomsmi/
---

Online shop with dried lavender bundles, essential oils, therapeutic sprays, healing salves, medicinal lip balms, wreaths, sachets and a plethora of aromas.