---
title: Dissident Rags
description: We are Dissidents. We are builders, creators, and warriors of a higher cause. We make beautiful things to share the message of defiance with a little levity and fashionable fortitude.
author: Smallygdala
date: 2022-09-07
tags:
  - shop
  - clothing
url: https://www.dissidentrags.com/
location: Online
profile: https://noagendasocial.com/@DangerousVariant
image:
---

Dissident Rags sells original art on apparel, prints, and other items designed by dissidents for dissidents living the dissident life. Stand in opposition - and support those who strive to live free by supporting our shop.

NA producers can use the one-time coupon code "ITM" at checkout to receive 10% off their entire order.
