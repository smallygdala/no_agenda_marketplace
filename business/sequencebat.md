---
title: The Sequence Bat
description: Baseball swing training tool
author: Smallygdala
date: 2022-05-24
tags:
  - shop
  - baseball
url: https://www.sequencebat.com/
location: Online
profile: https://www.instagram.com/sequencebat/
image:
---

The Sequence Bat is a valuable tool that creates a feel for proper spacing which allows hitting through a large zone to maximize our margin for error.
